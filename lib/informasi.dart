import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:xml/xml.dart' as xml;

class Informasi extends StatelessWidget {
  const Informasi({Key? key}) : super(key: key);

  final String apiUrl = "https://data.bmkg.go.id/DataMKG/TEWS/autogempa.xml";

  Future<xml.XmlDocument> fetchData() async {
    final response = await http.get(Uri.parse(apiUrl));
    if (response.statusCode == 200) {
      return xml.XmlDocument.parse(response.body);
    } else {
      throw Exception('Failed to fetch data');
    }
  }

  void LogOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: FutureBuilder<xml.XmlDocument>(
          future: fetchData(),
          builder:
              (BuildContext context, AsyncSnapshot<xml.XmlDocument> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(child: CircularProgressIndicator());
            } else if (snapshot.hasError) {
              return const Center(child: Text('Failed to fetch data'));
            } else {
              final data = snapshot.data;
              final gempa = data!.findAllElements('gempa').first;
              final tanggal = gempa.findElements('Tanggal').single.text;
              final jam = gempa.findElements('Jam').single.text;
              final dateTime = gempa.findElements('DateTime').single.text;
              final magnitudo = gempa.findElements('Magnitude').single.text;
              final kedalaman = gempa.findElements('Kedalaman').single.text;
              final koordinat = gempa
                  .findElements('point')
                  .single
                  .findElements('coordinates')
                  .single
                  .text;
              final lintang = gempa.findElements('Lintang').single.text;
              final bujur = gempa.findElements('Bujur').single.text;
              final lokasi = gempa.findElements('Wilayah').single.text;
              final potensi = gempa.findElements('Potensi').single.text;
              final dirasakan = gempa.findElements('Dirasakan').single.text;
              final shakemap = gempa.findElements('Shakemap').single.text;

              return Column(
                children: [
                  Container(
                    height: 140,
                    width: double.infinity,
                    color: Colors.blue,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        SizedBox(width: 10),
                        Text(
                          "DETEKSI GEMPA DINI",
                          style: GoogleFonts.robotoCondensed(
                            color: Colors.white,
                            fontSize: 23,
                          ),
                        ),
                        SizedBox(width: 10),
                      ],
                    ),
                  ),
                  Expanded(
                    child: ListView(
                      padding: const EdgeInsets.all(20),
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            const Icon(Icons.info_outline_rounded, size: 50),
                            const SizedBox(width: 5),
                            Text(
                              'INFORMASI',
                              style: GoogleFonts.robotoCondensed(
                                fontSize: 23,
                                textStyle: const TextStyle(
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 20),
                        Text(
                          'Tanggal: $tanggal',
                          style: GoogleFonts.robotoCondensed(fontSize: 14),
                        ),
                        Text(
                          'Jam: $jam',
                          style: GoogleFonts.robotoCondensed(fontSize: 14),
                        ),
                        Text(
                          'DateTime: $dateTime',
                          style: GoogleFonts.robotoCondensed(fontSize: 14),
                        ),
                        Text(
                          'Magnitudo: $magnitudo',
                          style: GoogleFonts.robotoCondensed(fontSize: 14),
                        ),
                        Text(
                          'Kedalaman: $kedalaman',
                          style: GoogleFonts.robotoCondensed(fontSize: 14),
                        ),
                        Text(
                          'Koordinat: $koordinat',
                          style: GoogleFonts.robotoCondensed(fontSize: 14),
                        ),
                        Text(
                          'Lintang: $lintang',
                          style: GoogleFonts.robotoCondensed(fontSize: 14),
                        ),
                        Text(
                          'Bujur: $bujur',
                          style: GoogleFonts.robotoCondensed(fontSize: 14),
                        ),
                        Text(
                          'Lokasi: $lokasi',
                          style: GoogleFonts.robotoCondensed(fontSize: 14),
                        ),
                        Text(
                          'Potensi: $potensi',
                          style: GoogleFonts.robotoCondensed(fontSize: 14),
                        ),
                        Text(
                          'Dirasakan: $dirasakan',
                          style: GoogleFonts.robotoCondensed(fontSize: 14),
                        ),
                        const SizedBox(height: 20),
                        Image.network(
                          'https://data.bmkg.go.id/DataMKG/TEWS/$shakemap',
                          fit: BoxFit.cover,
                        ),
                      ],
                    ),
                  ),
                ],
              );
            }
          },
        ),
      ),
    );
  }
}
