import 'package:flutter/material.dart';
import 'package:flutter_application_login/Logout.dart';
import 'package:flutter_application_login/evakuasi.dart';
import 'package:flutter_application_login/home_page.dart';
import 'package:flutter_application_login/informasi.dart';
import 'package:flutter_application_login/laporan.dart';
import 'package:flutter_application_login/maps.dart';

class BottomNav extends StatefulWidget {
  const BottomNav({super.key});

  @override
  _BottomNav createState() => _BottomNav();
}

class _BottomNav extends State<BottomNav> {
  int _currentIndex = 0;

  final List<Widget> _children = [const HomePage(),const Maps(),const Evakuasi(),const Laporan(),const Informasi(),const Logout(),];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        items: const [
          BottomNavigationBarItem(
             backgroundColor: Colors.blue,
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
           backgroundColor: Colors.blue,
            icon: Icon(Icons.map),
            label: 'Maps',
          ),
          BottomNavigationBarItem(
             backgroundColor: Colors.blue,
            icon: Icon(Icons.door_sliding),
            label: 'Evakuasi',
          ),
          BottomNavigationBarItem(
            backgroundColor: Colors.blue,
            icon: Icon(Icons.list),
            label: 'Lapor',
          ),
           BottomNavigationBarItem(
             backgroundColor: Colors.blue,
            icon: Icon(Icons.info),
            label: 'Informasi',
          ),
           BottomNavigationBarItem(
            backgroundColor: Colors.blue,
            icon: Icon(Icons.logout),
            label: 'Keluar',
          ),
        ],
      ),
    );
  }
}

