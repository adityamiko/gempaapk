import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Evakuasi extends StatelessWidget {
  const Evakuasi({super.key});

  void LogOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
   Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                    height: 150, width: double.infinity, color: Colors.blue),
                Column(
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        const SizedBox(
                          width: 30,
                        ),
                        Text(
                          "DETEKSI GEMPA DINI",
                          style:  GoogleFonts.kanit(
                            color: Colors.white,
                            fontSize: 30,
                          ),
                        ),
                        const SizedBox(
                          width: 30,
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        const Icon(Icons.map_sharp),
                        Text(
                          'JALUR EVAKUASI',
                          style:  GoogleFonts.robotoCondensed(
                            fontSize: 20,
                            textStyle: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        const SizedBox(width: 200),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Ikuti Petunjuk Arah dibawah ini untuk menuju tempat yang aman",
                      style: GoogleFonts.robotoCondensed(fontSize: 17),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Positioned(
                      child: Align(
                        child: SizedBox(
                          width: 20,
                          height: 50,
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(40),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      // more11a54 (212:64)
                      left: 20,
                      top: 19,
                      child: Align(
                        child: SizedBox(
                          width: 500,
                          height: 300,
                          child: Image.network(
                            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSDM4SPPZ1B9PSQceE-bjOvE5Rf2vGw3cHvdw&usqp=CAU',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
      ),
      ),
    );
    
  }
}
